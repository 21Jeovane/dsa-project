### A Pluto.jl notebook ###
# v0.18.2

using Markdown
using InteractiveUtils

# ╔═╡ 38dffb87-6c2e-444e-9c82-fa9fb9f43398
import os

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)

import cv2

import pickle

import matplotlib.pyplot as plt

import seaborn as sns

from tqdm import tqdm

from sklearn.preprocessing import OneHotEncoder

from sklearn.metrics import confusion_matrix

from keras.models import Model, load_model

from keras.layers import Dense, Input, Conv2D, MaxPool2D, Flatten

from keras.preprocessing.image import ImageDataGeneratornp.random.seed(22)


# ╔═╡ 93d4a7b5-181a-44ff-8b4b-446cdfb74eb5
def load_normal(norm_path):

norm_files = np.array(os.listdir(norm_path))

**norm_labels = np.array(['normal']*len(norm_files**))

norm_images = []

for image in tqdm(norm_files):

image = cv2.imread(norm_path + image)

image = cv2.resize(image, dsize=(200,200))

image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

norm_images.append(image)

	
	
norm_images = np.array(norm_images)

return norm_images, norm_labels



# ╔═╡ c5bf03f4-2cbe-4084-891d-3bf2310fec9c
def load_pneumonia(pneu_path):

pneu_files = np.array(os.listdir(pneu_path))

pneu_labels = np.array([pneu_file.split('_')[1] for pneu_file in pneu_files])

  

pneu_images = []

for image in tqdm(pneu_files):

image = cv2.imread(pneu_path + image)

image = cv2.resize(image, dsize=(200,200))

image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

pneu_images.append(image)

**return pneu_images, pneu_labels**

# ╔═╡ 523234ea-9bc5-4e3e-b4d6-4aa2c5d51bb6
norm_images, norm_labels = load_normal('/kaggle/input/chest-xray-pneumonia/chest_xray/train/NORMAL/')pneu_images, pneu_labels = load_pneumonia('/kaggle/input/chest-xray-pneumonia/chest_xray/train/PNEUMONIA/')

# ╔═╡ 927455f9-3da6-4972-9e7f-fc7f26e37f32
X_train = np.append(**norm_images**, **pneu_images**, axis=0)

y_train = np.append(**norm_labels**, **pneu_labels**)

# ╔═╡ 1da30686-027e-4964-bb77-bec9008e3f9a
x_train.shape

# ╔═╡ ed9e61e7-e00f-4e9b-88a7-8d4f225a1797
np.unique(y_train, return_counts=)

# ╔═╡ 7ce2b6a4-4549-42cd-a18e-9234c784c730
fig, axes = plt.subplots(ncols=7, nrows=2, figsize=(16, 4))

indices = np.random.choice(len(X_train), 14)

counter = 0

for i in range(2):

for j in range(7):

axes[i,j].set_title(y_train[indices[counter]])

axes[i,j].imshow(X_train[indices[counter]], cmap='gray')

axes[i,j].get_xaxis().set_visible(False)

axes[i,j].get_yaxis().set_visible(False)

counter += 1

plt.show()

# ╔═╡ 88aae694-90d5-4b84-a654-4f7cc55209cd
norm_images_test, norm_labels_test = load_normal('/kaggle/input/chest-xray-pneumonia/chest_xray/test/NORMAL/')pneu_images_test, pneu_labels_test = load_pneumonia('/kaggle/input/chest-xray-pneumonia/chest_xray/test/PNEUMONIA/')X_test = np.append(norm_images_test, pneu_images_test, axis=0)

# ╔═╡ fc666b65-e691-4711-ad37-95ac56f43fa3
with open('pneumonia_data.pickle', 'wb') as f:

pickle.dump((X_train, X_test, y_train, y_test), f)# Use this to load variables

with open('pneumonia_data.pickle', 'rb') as f:

(X_train, X_test, y_train, y_test) = pickle.load(f)

# ╔═╡ 78519dde-4db5-485e-8568-b066a5eccb7a
y_train = y_train[:, np.newaxis]

# ╔═╡ 50eec745-14ef-44de-a238-c21a79bbf8cc
y_train.shape

# ╔═╡ 71e3b79e-58b0-4ca5-8c2e-567feef69cab
one_hot_encoder = OneHotEncoder(sparse=False)

# ╔═╡ ce7d17fb-959e-46fc-8160-77f64432a39f
y_train_one_hot = one_hot_encoder.fit_transform(y_train)

# ╔═╡ fa77c500-f96b-4000-bf02-f67f0d1ab5ca
y_test_one_hot = one_hot_encoder.transform(y_test)

# ╔═╡ 15be6b35-7f0d-48ff-b948-a32f1453701d
y_test = y_test[:, np.newaxis]

# ╔═╡ ae1140b4-799a-45c3-9956-328a48cea81e
y_test = np.append(norm_labels_test, pneu_labels_test)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═38dffb87-6c2e-444e-9c82-fa9fb9f43398
# ╠═93d4a7b5-181a-44ff-8b4b-446cdfb74eb5
# ╠═c5bf03f4-2cbe-4084-891d-3bf2310fec9c
# ╠═523234ea-9bc5-4e3e-b4d6-4aa2c5d51bb6
# ╠═927455f9-3da6-4972-9e7f-fc7f26e37f32
# ╠═1da30686-027e-4964-bb77-bec9008e3f9a
# ╠═50eec745-14ef-44de-a238-c21a79bbf8cc
# ╠═ed9e61e7-e00f-4e9b-88a7-8d4f225a1797
# ╠═7ce2b6a4-4549-42cd-a18e-9234c784c730
# ╠═88aae694-90d5-4b84-a654-4f7cc55209cd
# ╠═ae1140b4-799a-45c3-9956-328a48cea81e
# ╠═fc666b65-e691-4711-ad37-95ac56f43fa3
# ╠═78519dde-4db5-485e-8568-b066a5eccb7a
# ╠═15be6b35-7f0d-48ff-b948-a32f1453701d
# ╠═71e3b79e-58b0-4ca5-8c2e-567feef69cab
# ╠═ce7d17fb-959e-46fc-8160-77f64432a39f
# ╠═fa77c500-f96b-4000-bf02-f67f0d1ab5ca
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
